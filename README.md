//Melinte Paul-Eduard
//336CC

# Tema 3 Page Loader

Am rezolvat problema de demand paging prin scrierea unei functii handler pentru semnalul SIGSEGV, page_handler. Pasii
functiei:
    Verificam ca am primit semnalul bun,SIGSEGV
    Identificam segmentul de date din exec->segments care corespunde adresei page-fault-ului
    Daca nu identificam acest segment, rulam handler-ul default 
    Daca il identificam, calculam in a cata pagina se afla fault-ul
    Daca pagina a fost mapata si tot avem fault, inseamna ca nu i s-au dat permisiunile necesare
    Daca nu a fost mapata pagina, o mapam si setam 1 in vectorul seg->data
    Setam toti bytetii la 0, daca avem date de citit in pagina le citim din executabil
    Setam permisiunile paginii, daca le-am seta de la inceput, s-ar putea sa nu putem scrie in pagina

Surse:
    Pagina de man a mmap
    Laboratorul de paginare

