/*
 * Loader Implementation
 *
 * 2018, Operating Systems
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/mman.h>

#include "exec_parser.h"

static so_exec_t *exec;
static int page_size;
static struct sigaction SIGSEGV_old_action;
static int fd_exec;

static void page_handler(int signum, siginfo_t *info, void *context) {
    if (signum != SIGSEGV) {
        SIGSEGV_old_action.sa_sigaction(signum, info, context);
        return;
    }


    so_seg_t *seg = NULL;
    void *addr = info->si_addr;

    //Finding the data segment
    for (int i = 0; i < exec->segments_no; i++) {
        if (exec->segments[i].vaddr <= addr && exec->segments[i].vaddr + exec->segments[i].mem_size > addr) {
            seg = &(exec->segments[i]);
        }
    }

    //If not fount, use the old signal handler
    if (seg == NULL) {
        SIGSEGV_old_action.sa_sigaction(signum, info, context);
        return;
    }

    //we will use the void* data pointer to allocate an byte vector
    if (seg->data == NULL) {
        //if data[i]==0 means the i-th page hasnt been loaded yet, if 1 it has been loaded
        seg->data = calloc((seg->mem_size / page_size + 1), sizeof(char));
    }

    long page = (long) (addr - seg->vaddr) / page_size;

    if (((char *) seg->data)[page] == 1) {

        //if we get a pagefault for a page that has been loaded, it means its permissions werent enough
        SIGSEGV_old_action.sa_sigaction(signum, info, context);
        return;
    }

    //If the page wasnt mapped, we map it, with write perms
    void *page_addr = mmap((void *) (seg->vaddr + page * page_size), page_size, PROT_WRITE,
                           MAP_PRIVATE | MAP_FIXED | MAP_ANONYMOUS, 0, 0);

    if (page_addr != (void *) (seg->vaddr + page * page_size)) {
        SIGSEGV_old_action.sa_sigaction(signum, info, context);
        return;
    }

    ((char *) seg->data)[page] = 1;

    memset(page_addr, 0, page_size);

    //We write the page, if it has written data from the file
    if (page_addr < seg->vaddr + seg->file_size) {
        lseek(fd_exec, seg->offset + page * page_size, SEEK_SET);
        if (page_addr + page_size > seg->vaddr + seg->file_size) {
            read(fd_exec, page_addr, seg->file_size % page_size);
        } else { read(fd_exec, page_addr, page_size); }
    }

    //We change the permissions to the seg values
    mprotect(page_addr, page_size, seg->perm);
}

int so_init_loader(void) {
    page_size = getpagesize();

    struct sigaction action;
    int rc;

    //signal handler initializations
    action.sa_sigaction = page_handler;
    sigemptyset(&action.sa_mask);
    sigaddset(&action.sa_mask, SIGSEGV);
    action.sa_flags = SA_SIGINFO;

    rc = sigaction(SIGSEGV, &action, &SIGSEGV_old_action);
    if (rc == -1) {
        return -1;
    }

    return 0;
}

int so_execute(char *path, char *argv[]) {
    //We open a file descriptor so we can read from the file
    fd_exec = open(path, O_RDONLY);
    if (fd_exec == 0) {
        return -1;
    }

    exec = so_parse_exec(path);
    if (!exec)
        return -1;

    so_start_exec(exec, argv);

    return -1;
}
